import React from "react";
import Aux from "../../../hoc/Aux/Aux";
import PropTypes from "prop-types";
import Button from "../../UI/Button/Button";

const orderSummary = (props) => {
  const ingredientSummary = Object.keys(props.ingredients).map((item, i) => (
    <li key={i}>
      <span style={{ textTransform: "capitalize" }}>{item}</span>:{" "}
      {props.ingredients[item]}
    </li>
  ));

  return (
    <Aux>
      <h3>Your Order:</h3>
      <p>Delicious burger with following ingredients:</p>
      <ul>{ingredientSummary}</ul>
      <p>
        <strong>Total Price: </strong>
        {props.price.toFixed(2)}
      </p>
      <p>Continue to Checkout?</p>
      <div style={{ display: "flex" }}>
        <Button clicked={props.cancelOrder} btnType="Danger">
          CANCEL
        </Button>
        <Button btnType="Success" clicked={props.continueOrder}>
          CONTINUE
        </Button>
      </div>
    </Aux>
  );
};

orderSummary.propTypes = {
  ingredients: PropTypes.object,
  cancelOrder: PropTypes.func,
  continueOrder: PropTypes.func,
  price: PropTypes.number,
};

export default orderSummary;
