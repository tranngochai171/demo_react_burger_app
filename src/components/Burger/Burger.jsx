import React from "react";
import BurgerIngredient from "./BurgerIngredient/BurgerIngredient";
import classes from "./Burger.module.css";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";

const burger = (props) => {
  let burgerIngredient = [];
  let totalIngredient = 0;
  for (const [ingredient, amount] of Object.entries(props.ingredients)) {
    if (amount > 0) {
      for (let i = 0; i < amount; i++) {
        burgerIngredient.push(
          <BurgerIngredient key={`${ingredient}_${i}`} type={ingredient} />
        );
        totalIngredient++;
      }
    }
  }
  const isNoIngredient = totalIngredient === 0;
  return (
    <div className={classes.Burger}>
      <h2>Total price for this burger: {+props.totalPrice.toFixed(2)}</h2>
      <BurgerIngredient type="bread-top" />
      {burgerIngredient}
      <BurgerIngredient type="bread-bottom" />
      {isNoIngredient && <h1>Please start adding Ingredients!</h1>}
    </div>
  );
};

burger.propTypes = {
  ingredients: PropTypes.object,
  totalPrice: PropTypes.number,
};

export default withRouter(burger);
