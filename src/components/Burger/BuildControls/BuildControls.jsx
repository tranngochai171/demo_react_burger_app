import React from "react";
import PropTypes from "prop-types";
import Aux from "../../../hoc/Aux/Aux";
import BuildControl from "./BuildControl/BuildControl";
import classes from "./BuildControls.module.css";

const controls = [
  { label: "Salad", type: "salad" },
  { label: "Bacon", type: "bacon" },
  { label: "Cheese", type: "cheese" },
  { label: "Meat", type: "meat" },
];

const buildControls = (props) => {
  return (
    <Aux>
      <div className={classes.BuildControls}>
        {controls.map((item, index) => (
          <BuildControl
            key={index}
            control={item}
            addIngredient={props.addIngredient}
            disabled={props.disabled[item.type]}
          />
        ))}
        <button
          disabled={!props.purchaseable}
          className={classes.OrderButton}
          onClick={props.ordered}
        >
          {props.isAuthenticated ? "ORDER NOW" : "SIGN UP TO ORDER"}
        </button>
      </div>
    </Aux>
  );
};

buildControls.propTypes = {
  addIngredient: PropTypes.func.isRequired,
  disabled: PropTypes.object,
  purchaseable: PropTypes.bool,
  ordered: PropTypes.func,
};

export default buildControls;
