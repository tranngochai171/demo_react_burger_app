import React from "react";
import PropTypes from "prop-types";
import classes from "./BuildControl.module.css";
const buildControl = (props) => (
  <div className={classes.BuildControl}>
    <div className={classes.Label}>{props.control.label}</div>
    <button
      disabled={props.disabled}
      className={classes.Less}
      onClick={() =>
        props.addIngredient(props.control.type, false /* isIncreasing */)
      }
    >
      Less
    </button>
    <button
      className={classes.More}
      onClick={() =>
        props.addIngredient(props.control.type, true /* isIncreasing */)
      }
    >
      More
    </button>
  </div>
);
buildControl.propTypes = {
  control: PropTypes.object,
  addIngredient: PropTypes.func,
  disabled: PropTypes.bool,
};
export default buildControl;
