import React from "react";
import Burger from "../../Burger/Burger";
import Button from "../../UI/Button/Button";
import classes from "./CheckoutSummary.module.css";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";

const checkoutSummary = (props) => {
  // eslint-disable-next-line
  const goBack = () => {
    props.history.goBack();
  };
  return (
    <div className={classes.CheckoutSummary}>
      <h1>We hope it tastes well!!!</h1>
      <div style={{ width: "100%", margin: "auto" }}>
        <Burger
          ingredients={props.ingredients}
          totalPrice={+props.totalPrice}
        />
      </div>
      <Button btnType="Danger" clicked={props.checkoutCancelled}>
        CANCEL
      </Button>
      <Button btnType="Success" clicked={props.checkoutContinued}>
        CONTINUE
      </Button>
    </div>
  );
};

checkoutSummary.propTypes = {
  ingredients: PropTypes.object,
  totalPrice: PropTypes.string,
  checkoutCancelled: PropTypes.func,
  checkoutContinued: PropTypes.func,
};

export default withRouter(checkoutSummary);
