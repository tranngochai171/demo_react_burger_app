import React from "react";
import classes from "./Order.module.css";

const order = (props) => {
  const renderIngredients = (ingredients) => {
    const render = [];
    for (const item in ingredients) {
      render.push({
        name: item,
        amount: ingredients[item],
      });
    }
    return render.map((item, i) => (
      <span
        key={i}
        className={classes.SpanOrder}
      >{`${item.name} (${item.amount})`}</span>
    ));
  };
  return (
    <div className={classes.Order}>
      <p>Ingredients: {renderIngredients(props.ingredients)}</p>
      <p>
        Price: <strong>USD {props.price}</strong>
      </p>
    </div>
  );
};

export default order;
