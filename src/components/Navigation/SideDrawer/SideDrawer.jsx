import React from "react";
import Logo from "../../Logo/Logo";
import NavigationItems from "../NavigationItems/NavigationItems";
import Backdrop from "../../UI/Backdrop/Backdrop";
import Aux from "../../../hoc/Aux/Aux";
import PropTypes from "prop-types";
import classes from "./SideDrawer.module.css";
import { useSelector } from "react-redux";

const SideDrawer = (props) => {
  const isAuthenticated = useSelector((state) => !!state.auth.token);
  return (
    <Aux>
      <Backdrop show={props.open} clicked={props.closed} />
      <div
        className={[
          classes.SideDrawer,
          props.open ? classes.Open : classes.Close,
        ].join(" ")}
        onClick={props.closed}
      >
        <Logo height="11%" className={classes.Logo} />
        <nav>
          <NavigationItems isAuthenticated={isAuthenticated} />
        </nav>
      </div>
    </Aux>
  );
};

SideDrawer.propTypes = {
  closed: PropTypes.func,
  open: PropTypes.bool,
};

export default SideDrawer;
