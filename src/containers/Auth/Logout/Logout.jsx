import React, { Component } from "react";
import { connect } from "react-redux";
import * as actionCreators from "../../../store/actions/actionCreators";
import { Redirect } from "react-router-dom";

class Logout extends Component {
  componentDidMount() {
    this.props.setAuthRedirectPath();
    this.props.onLogout();
  }
  render() {
    return <Redirect to="/" />;
  }
}

const mapDispatchToProps = (dispatch) => ({
  onLogout: () => dispatch(actionCreators.logout()),
  setAuthRedirectPath: () => dispatch(actionCreators.setAuthRedirectPath("/")),
});

export default connect(null, mapDispatchToProps)(Logout);
