import Layout from "./Layout/Layout";
import BurgerBuilder from "./BurgurBuilder/BurgerBuilder";
import { Route, Switch, Redirect } from "react-router-dom";
import Logout from "./Auth/Logout/Logout";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import * as actionCreators from "../store/actions/actionCreators";
import asyncComponent from "../hoc/asyncComponent/asyncComponent";

const asyncCheckout = asyncComponent(() => import("./Checkout/Checkout"));
const asyncOrder = asyncComponent(() => import("./Orders/Orders"));
const asyncAuth = asyncComponent(() => import("./Auth/Auth"));

const App = () => {
  const dispatch = useDispatch();
  const isAuthenticated = useSelector((state) => !!state.auth.token);
  useEffect(() => {
    dispatch(actionCreators.authCheckState());
  }, [dispatch]);
  let route = (
    <Switch>
      <Route path="/auth" component={asyncAuth} />
      <Route path="/" exact component={BurgerBuilder} />
      <Redirect to="/" />
    </Switch>
  );
  if (isAuthenticated) {
    route = (
      <Switch>
        <Route path="/auth" component={asyncAuth} />
        <Route path="/orders" component={asyncOrder} />
        <Route path="/checkout" component={asyncCheckout} />
        <Route path="/logout" component={Logout} />
        <Route path="/" exact component={BurgerBuilder} />
      </Switch>
    );
  }
  return (
    <div>
      <Layout>
        <Switch>{route}</Switch>
      </Layout>
    </div>
  );
};

export default App;
