import React, { Component } from "react";
import Order from "../../components/Order/Order";
import axios from "../../axios-orders";
import Spinner from "../../components/UI/Spinner/Spinner";
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";
import { connect } from "react-redux";
import * as actionCreators from "../../store/actions/actionCreators";

class OrderComponent extends Component {
  componentDidMount() {
    this.props.fetchOrders(this.props.token, this.props.userId);
  }

  renderOrders = () => {
    return this.props.orders.map((order) => (
      <Order
        key={order.id}
        ingredients={order.ingredients}
        price={order.price}
      />
    ));
  };

  render() {
    let orders = <Spinner />;
    if (!this.props.isLoading) {
      orders = this.renderOrders();
    }
    return <div>{orders}</div>;
  }
}
const mapStateToProps = (state) => ({
  orders: state.order.orders,
  isLoading: state.order.isLoading,
  token: state.auth.token,
  userId: state.auth.userId,
});

const mapDispatchToProps = (dispatch) => ({
  fetchOrders: (token, userId) =>
    dispatch(actionCreators.asyncFetchOrders(token, userId)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(OrderComponent, axios));
