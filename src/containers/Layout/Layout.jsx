import React, { useState } from "react";
import Aux from "../../hoc/Aux/Aux";
import classes from "./Layout.module.css";
import Toolbar from "../../components/Navigation/Toolbar/Toolbar";
import SideDrawer from "../../components/Navigation/SideDrawer/SideDrawer";
import { useSelector } from "react-redux";

const Layout = (props) => {
  const [show, setShow] = useState(false);
  const sideDrawerCloseHandler = () => {
    setShow(false);
  };
  const sideDrawerToggleHandler = () => {
    setShow((prevState) => !prevState);
  };
  const isAuthenticated = useSelector((state) => !!state.auth.token);
  return (
    <Aux>
      <Toolbar
        openSideDrawer={sideDrawerToggleHandler}
        isAuthenticated={isAuthenticated}
      />
      <SideDrawer closed={sideDrawerCloseHandler} open={show} />
      <main className={classes.Content}>{props.children}</main>
    </Aux>
  );
};

export default Layout;
