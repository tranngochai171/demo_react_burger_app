import React, { Component } from "react";
import Aux from "../../hoc/Aux/Aux";
import Burger from "../../components/Burger/Burger";
import BuildControls from "../../components/Burger/BuildControls/BuildControls";
import Modal from "../../components/UI/Modal/Modal";
import OrderSummary from "../../components/Burger/OrderSummary/OrderSummary";
import Spinner from "../../components/UI/Spinner/Spinner";
import axios from "../../axios-orders";
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";
import { connect } from "react-redux";
import * as actionCreators from "../../store/actions/actionCreators";

class BurgerBuilder extends Component {
  state = {
    purchasing: false,
  };

  componentDidMount() {
    this.props.fetchIngredients();
  }

  updatePurchaseState = (ingredients) => {
    const sum = Object.keys(ingredients)
      .map((key) => {
        return ingredients[key];
      })
      .reduce((sum, item) => sum + item, 0);
    return sum > 0;
  };

  purchaseHandler = () => {
    if (this.props.isAuthenticated) {
      this.setState((prevState) => ({ purchasing: true }));
    } else {
      this.props.onSetAuthRedirectPath("/checkout");
      this.props.history.push("/auth");
    }
  };

  purchaseCancelHandler = () => {
    this.setState({ purchasing: false });
  };

  purchaseContinueHandler = () => {
    this.props.onInitPurchase();
    this.props.history.push({
      pathname: "/checkout",
    });
  };

  // setQueryForCheckout = () => {
  //   let arr = [];
  //   for (let key in this.props.ingredients) {
  //     arr.push(
  //       `${encodeURIComponent(key)}=${encodeURIComponent(
  //         this.props.ingredients[key]
  //       )}`
  //     );
  //   }
  //   arr.push(`price=${this.props.totalPrice}`);
  //   return arr.join("&");
  // };

  render() {
    const disabledInfo = { ...this.props.ingredients };
    for (let key in disabledInfo) {
      disabledInfo[key] = disabledInfo[key] <= 0;
    }
    let orderSummary = null;
    let burger = this.props.isError ? (
      <p>Ingredients can't be loaded</p>
    ) : (
      <Spinner />
    );
    if (this.props.ingredients) {
      burger = (
        <Aux>
          <Burger
            ingredients={this.props.ingredients}
            totalPrice={this.props.totalPrice}
          />
          <BuildControls
            addIngredient={this.props.onAddingIngredient}
            disabled={disabledInfo}
            purchaseable={this.updatePurchaseState(this.props.ingredients)}
            ordered={this.purchaseHandler}
            isAuthenticated={this.props.isAuthenticated}
          />
        </Aux>
      );
      if (this.state.purchasing)
        orderSummary = (
          <OrderSummary
            price={this.props.totalPrice}
            ingredients={this.props.ingredients}
            cancelOrder={this.purchaseCancelHandler}
            continueOrder={this.purchaseContinueHandler}
          />
        );
    }
    if (this.props.isLoading) {
      orderSummary = <Spinner />;
    }
    return (
      <Aux>
        <Modal
          show={this.state.purchasing}
          clicked={this.purchaseCancelHandler}
        >
          {orderSummary}
        </Modal>
        {burger}
      </Aux>
    );
  }
}
const mapStateToProps = (state) => ({
  ingredients: state.ing.ingredients,
  totalPrice: state.ing.totalPrice,
  isLoading: state.ing.isLoading,
  isError: state.ing.isError,
  isAuthenticated: !!state.auth.token,
});

const mapDispatchToProps = (dispatch) => ({
  updateIngredient: (ingredients) =>
    dispatch(actionCreators.updateIngredient(ingredients)),
  onAddingIngredient: (ingredient, isIncreasing) =>
    dispatch(actionCreators.onAddingIngredient(ingredient, isIncreasing)),
  fetchIngredients: () => dispatch(actionCreators.asyncFetchIngredients()),
  onInitPurchase: () => dispatch(actionCreators.purchaseInit()),
  onSetAuthRedirectPath: (path) =>
    dispatch(actionCreators.setAuthRedirectPath(path)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(BurgerBuilder, axios));
