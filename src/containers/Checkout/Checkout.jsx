import React, { Component } from "react";
import CheckoutSummary from "../../components/Order/CheckoutSummary/CheckoutSummary";
import { Route, Redirect } from "react-router-dom";
import ContactData from "./ContactData/ContactData";
import { connect } from "react-redux";

class Checkout extends Component {
  onCancel = () => {
    this.props.history.goBack();
  };

  onContinue = () => {
    this.props.history.replace("/checkout/contact-data");
  };

  // componentDidMount() {
  //   const ingredientsArr = new URLSearchParams(this.props.location.search);
  //   const ingredients = {};
  //   let price = 0;
  //   for (let [key, value] of ingredientsArr.entries()) {
  //     if (key === "price") {
  //       price = value;
  //     } else {
  //       ingredients[key] = +value;
  //     }
  //   }
  //   this.setState({ ingredients, totalPrice: price });
  // }

  render() {
    let summary = <Redirect to="/" />;

    if (this.props.ingredients) {
      const purchasedRedirect = this.props.purchased ? (
        <Redirect to="/" />
      ) : null;
      summary = (
        <div>
          {purchasedRedirect}
          <CheckoutSummary
            ingredients={this.props.ingredients}
            totalPrice={this.props.totalPrice}
            checkoutCancelled={this.onCancel}
            checkoutContinued={this.onContinue}
          />
          <Route
            path={`${this.props.match.path}/contact-data`}
            render={(props) => (
              <ContactData
                ingredients={this.props.ingredients}
                price={+this.props.totalPrice}
                {...props}
              />
            )}
          />
        </div>
      );
    }
    return summary;
  }
}

const mapStateToProps = (state) => ({
  ingredients: state.ing.ingredients,
  totalPrice: state.ing.totalPrice,
  purchased: state.order.purchased,
});

export default connect(mapStateToProps)(Checkout);
