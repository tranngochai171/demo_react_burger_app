import React, { Component } from "react";
import Button from "../../../components/UI/Button/Button";
import classes from "./ContactData.module.css";
import axios from "../../../axios-orders";
import Spinner from "../../../components/UI/Spinner/Spinner";
import Input from "../../../components/UI/Input/Input";
import { connect } from "react-redux";
import withErrorHandler from "../../../hoc/withErrorHandler/withErrorHandler";
import * as actionCreators from "../../../store/actions/actionCreators";
import { checkValidity } from "../../../util/function/utility";
class ContactDataComponent extends Component {
  state = {
    orderForm: {
      name: {
        elementType: "input",
        elementConfig: {
          type: "text",
          placeholder: "Your name",
        },
        value: "",
        validation: {
          required: true,
        },
        valid: false,
        touched: false,
      },
      street: {
        elementType: "input",
        elementConfig: {
          type: "text",
          placeholder: "Street",
        },
        value: "",
        validation: {
          required: true,
        },
        valid: false,
        touched: false,
      },
      zipCode: {
        elementType: "input",
        elementConfig: {
          type: "text",
          placeholder: "ZIP Code",
        },
        value: "",
        validation: {
          required: true,
        },
        valid: false,
        touched: false,
      },
      country: {
        elementType: "input",
        elementConfig: {
          type: "text",
          placeholder: "Country",
        },
        value: "",
        validation: {
          required: true,
        },
        valid: false,
        touched: false,
      },
      email: {
        elementType: "input",
        elementConfig: {
          type: "email",
          placeholder: "Your E-mail",
        },
        value: "",
        validation: {
          required: true,
          isEmail: true,
        },
        valid: false,
        touched: false,
      },
      deliveryMethod: {
        elementType: "select",
        elementConfig: {
          options: [
            {
              value: "fastest",
              displayValue: "Fastest",
            },
            {
              value: "cheapest",
              displayValue: "Cheapest",
            },
          ],
        },
        value: "fastest",
        valid: true,
      },
    },
    formIsValid: false,
  };

  handleOnChange = (e, name) => {
    const orderForm = { ...this.state.orderForm };
    const updateFormElement = { ...orderForm[name] };
    updateFormElement.value = e.target.value;
    updateFormElement.valid = checkValidity(
      updateFormElement.value,
      updateFormElement.validation
    );
    updateFormElement.touched = true;
    orderForm[name] = updateFormElement;

    let formIsValid = true;
    for (let inputIdentifier in orderForm) {
      formIsValid = orderForm[inputIdentifier].valid && formIsValid;
    }

    this.setState({ orderForm, formIsValid });
  };

  orderHandler = (event) => {
    event.preventDefault();
    const formData = {};
    if (this.state.formIsValid) {
      for (let key in this.state.orderForm) {
        formData[key] = this.state.orderForm[key].value;
      }
      const order = {
        ingredients: this.props.ingredients,
        price: (+this.props.totalPrice).toFixed(2),
        orderData: formData,
        userId: this.props.userId,
      };

      this.props.handleSubmit(order, this.props.token);
      // this.setState({ loading: true });
      // axios
      //   .post("/orders.json", order)
      //   .then((response) => {
      //     this.setState({ loading: false });
      //     this.props.history.push("/");
      //   })
      //   .catch((error) => {
      //     this.setState({ loading: false });
      //     console.log(error);
      //   });
    }
  };

  render() {
    const formElementsArray = [];
    for (let key in this.state.orderForm) {
      formElementsArray.push({ id: key, config: this.state.orderForm[key] });
    }
    let form = (
      <form onSubmit={this.orderHandler}>
        {formElementsArray.map((formElement) => (
          <Input
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            onChange={(e) => this.handleOnChange(e, formElement.id)}
            invalid={!formElement.config.valid}
            shouldValidate={formElement.config.validation}
            touched={formElement.config.touched}
          />
        ))}
        <Button
          btnType="Success"
          clicked={this.orderHandler}
          disabled={!this.state.formIsValid}
        >
          ORDER HERE
        </Button>
      </form>
    );

    if (this.props.isLoading) {
      form = <Spinner />;
    }

    return (
      <div className={classes.ContactData}>
        <h4>Enter your Contact Data</h4>
        {form}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  ingredients: state.ing.ingredients,
  totalPrice: state.ing.totalPrice,
  isLoading: state.order.isLoading,
  isError: state.order.isError,
  userId: state.auth.userId,
  token: state.auth.token,
});

const mapDispatchToProps = (dispatch) => ({
  handleSubmit: (order, token) =>
    dispatch(actionCreators.asyncPurchaseBurger(order, token)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(ContactDataComponent, axios));
