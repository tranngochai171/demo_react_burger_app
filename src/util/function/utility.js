export const updateObject = (oldState, updateObject) => ({
  ...oldState,
  ...updateObject,
});

export const checkValidity = (value, rules) => {
  let isValid = true;
  if (rules?.required) {
    isValid = value.trim() !== "" && isValid;
  }
  if (rules?.isEmail) {
    // eslint-disable-next-line
    isValid = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(value.trim()) && isValid;
  }
  return isValid;
};
