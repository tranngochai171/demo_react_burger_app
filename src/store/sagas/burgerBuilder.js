import * as actions from "../actions/actionCreators";
import axios from "../../axios-orders";
import { put } from "redux-saga/effects";

export function* fetchIngredientsSaga(action) {
  yield put(actions.fetchIngredientsInit());
  try {
    const result = yield axios.get("/ingredients.json");
    yield put(actions.fetchIngredientsSuccess(result.data));
  } catch (error) {
    yield put(actions.fetchIngredientsFail());
  }
}
