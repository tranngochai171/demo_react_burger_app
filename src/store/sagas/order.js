import * as actions from "../actions/actionCreators";
import { put } from "redux-saga/effects";
import axios from "../../axios-orders";

export function* purchaseBurgerSaga({ type, payload }) {
  yield put(actions.purchaseBurgerInit());
  try {
    const result = yield axios.post(
      `/orders.json?auth=${payload.token}`,
      payload.order
    );
    yield put(actions.purchaseBurgerSuccess(result.data.name, payload.order));
  } catch (error) {
    yield put(actions.purchaseBurgerFail());
  }
}

export function* fetchOrdersSaga({ payload }) {
  yield put(actions.purchaseBurgerInit());
  try {
    const queryParam = `?auth=${payload.token}&orderBy="userId"&equalTo="${payload.userId}"`;
    const result = yield axios.get("/orders.json" + queryParam);
    const orders = [];
    for (let key in result.data) {
      orders.push({ ...result.data[key], id: key });
    }
    yield put(actions.fetchOrderSuccess(orders));
  } catch (error) {
    yield put(actions.purchaseBurgerFail());
  }
}
