import { put, call, delay } from "redux-saga/effects";
import * as actions from "../actions/actionCreators";
import axios from "axios";
import errorCode from "../../util/constants/errorCode.json";

export function* logoutSaga(action) {
  // yield localStorage.removeItem("token");
  // yield localStorage.removeItem("expirationDate");
  // yield localStorage.removeItem("userId");
  yield call([localStorage, "removeItem"], "token");
  yield call([localStorage, "removeItem"], "expirationDate");
  yield call([localStorage, "removeItem"], "userId");
  yield put(actions.logoutSucceed());
}

export function* checkAuthTimeoutSaga(action) {
  yield delay(action.payload.expirationTime * 1000);
  yield put(actions.logout());
}

export function* authUserSaga(action) {
  yield put(actions.authInit());
  const { email, password, isSignUp } = action.payload;
  try {
    const authData = {
      email,
      password,
      returnSecureToken: true,
    };
    const url = `https://identitytoolkit.googleapis.com/v1/accounts:${
      isSignUp ? "signUp" : "signInWithPassword"
    }?key=AIzaSyBhhbiAfV_5QfhH_cJXA7JApfXsJkiUDqY`;
    const result = yield axios.post(url, authData);
    yield put(actions.authSuccess(result.data.idToken, result.data.localId));
    const expirationDate = new Date(
      new Date().getTime() + result.data.expiresIn * 1000
    );
    yield localStorage.setItem("token", result.data.idToken);
    yield localStorage.setItem("expirationDate", expirationDate);
    yield localStorage.setItem("userId", result.data.localId);
    yield put(actions.asyncCheckAuthTimeout(result.data.expiresIn));
  } catch (error) {
    yield put(
      actions.authFail(errorCode[error?.response?.data?.error?.message])
    );
  }
}

export function* authCheckStateSaga(action) {
  const token = yield localStorage.getItem("token");
  if (!token) {
    yield put(actions.logout());
  } else {
    const expirationDate = new Date(localStorage.getItem("expirationDate"));
    if (expirationDate <= new Date()) {
      yield put(actions.logout());
    } else {
      const userId = yield localStorage.getItem("userId");
      yield put(actions.authSuccess(token, userId));
      yield put(
        actions.asyncCheckAuthTimeout(
          (expirationDate.getTime() - new Date().getTime()) / 1000
        )
      );
    }
  }
}
