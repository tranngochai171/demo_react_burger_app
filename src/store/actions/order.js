import * as actionTypes from "./actionTypes";
import axios from "../../axios-orders";

export const purchaseBurgerInit = () => ({
  type: actionTypes.PURCHASE_BURGER_START,
});

export const purchaseBurgerSuccess = (orderId, orderData) => ({
  type: actionTypes.PURCHASE_BURGER_SUCCESS,
  payload: {
    orderId,
    orderData,
  },
});

export const purchaseInit = () => ({
  type: actionTypes.PURCHASE_INIT,
});

export const purchaseBurgerFail = () => ({
  type: actionTypes.PURCHASE_BURGER_FAIL,
});

// const fetchOrderInit = () => ({
//   type: actionTypes.FETCH_ORDER_INIT,
// });

export const fetchOrderSuccess = (order) => ({
  type: actionTypes.FETCH_ORDER_SUCCESS,
  payload: order,
});

// const fetchOrderFail = () => ({
//   type: actionTypes.FETCH_ORDER_FAIL,
// });

// export const asyncPurchaseBurger = (order) => async (dispatch, getState) => {
//   dispatch(purchaseBurgerInit());
//   try {
//     const result = await axios.post(
//       `/orders.json?auth=${getState().auth.token}`,
//       order
//     );
//     dispatch(purchaseBurgerSuccess(result.data.name, order));
//   } catch (error) {
//     dispatch(purchaseBurgerFail());
//   }
// };

export const asyncPurchaseBurger = (order, token) => ({
  type: actionTypes.PURCHASE_BURGER,
  payload: {
    token,
    order,
  },
});

export const asyncFetchOrders = (token, userId) => ({
  type: actionTypes.FETCH_ORDER,
  payload: {
    token,
    userId,
  },
});
