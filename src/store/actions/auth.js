import * as actionTypes from "./actionTypes";

export const authInit = () => ({
  type: actionTypes.AUTH_INIT,
});

export const authSuccess = (token, userId) => ({
  type: actionTypes.AUTH_SUCCESS,
  payload: {
    token,
    userId,
  },
});
export const authFail = (errorMessage) => ({
  type: actionTypes.AUTH_FAIL,
  payload: errorMessage,
});

export const logout = () => {
  // localStorage.removeItem("token");
  // localStorage.removeItem("expirationDate");
  // localStorage.removeItem("userId");
  return {
    type: actionTypes.AUTH_INITIATE_LOGOUT,
  };
};

export const logoutSucceed = () => ({
  type: actionTypes.AUTH_LOGOUT,
});

export const asyncCheckAuthTimeout = (expirationTimeout) => {
  return {
    type: actionTypes.AUTH_CHECK_TIMEOUT,
    payload: {
      expirationTime: expirationTimeout,
    },
  };
  // setTimeout(() => {
  //   dispatch(logout());
  // }, expirationTimeout * 1000);
};

export const setAuthRedirectPath = (redirectPath) => ({
  type: actionTypes.SET_AUTH_REDIRECT_PATH,
  payload: redirectPath,
});

export const asyncAuth = (email, password, isSignUp) => ({
  type: actionTypes.AUTH_USER,
  payload: {
    email,
    password,
    isSignUp,
  },
});

export const authCheckState = () => {
  return {
    type: actionTypes.AUTH_CHECK_STATE,
  };
};
