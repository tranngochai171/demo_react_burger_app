import * as actionTypes from "./actionTypes";
import axios from "../../axios-orders";

export const updateIngredient = (ingredients) => ({
  type: actionTypes.GET_INGREDIENT,
  payload: ingredients,
});

export const onAddingIngredient = (ingredient, isIncreasing) => ({
  type: actionTypes.UPDATE_INGREDIENT,
  payload: {
    ingredient,
    isIncreasing,
  },
});

export const fetchIngredientsInit = () => ({
  type: actionTypes.FETCH_INGREDIENTS_INIT,
});
export const fetchIngredientsSuccess = (data) => ({
  type: actionTypes.FETCH_INGREDIENTS_SUCCESS,
  payload: data,
});
export const fetchIngredientsFail = () => ({
  type: actionTypes.FETCH_INGREDIENTS_FAIL,
});

export const asyncFetchIngredients = () => ({
  type: actionTypes.INIT_INGREDIENT,
});
