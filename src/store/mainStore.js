import { combineReducers } from "redux";
import burgerBuilderReducer from "./reducers/burgerBuilderReducer";
import orderReducer from "./reducers/orderReducer";
import authReducer from "./reducers/authReducer";

const mainStore = combineReducers({
  ing: burgerBuilderReducer,
  order: orderReducer,
  auth: authReducer,
});

export default mainStore;
