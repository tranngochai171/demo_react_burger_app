import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../util/function/utility";
const initialState = {
  isLoading: false,
  error: null,
  token: null,
  userId: null,
  authRedirectPath: "/",
};

const authStart = (state) => {
  return updateObject(state, { isLoading: true, error: null });
};

const authSuccess = (state, payload) => {
  return updateObject(state, {
    isLoading: false,
    error: null,
    token: payload.token,
    userId: payload.userId,
  });
};

const authLogout = (state) => {
  return updateObject(state, { token: null, userId: null });
};

const authFail = (state, payload) => {
  return updateObject(state, { isLoading: false, error: payload });
};

const setAuthRedirectPath = (state, payload) => {
  return updateObject(state, { authRedirectPath: payload });
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionTypes.AUTH_INIT:
      return authStart(state);
    case actionTypes.AUTH_SUCCESS:
      return authSuccess(state, payload);
    case actionTypes.AUTH_FAIL:
      return authFail(state, payload);
    case actionTypes.AUTH_LOGOUT:
      return authLogout(state);
    case actionTypes.SET_AUTH_REDIRECT_PATH:
      return setAuthRedirectPath(state, payload);
    default:
      return state;
  }
};

export default reducer;
