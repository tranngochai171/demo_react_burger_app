import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../util/function/utility";

const initialState = {
  orders: [],
  isLoading: false,
  isError: false,
  purchased: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.PURCHASE_INIT:
      return updateObject(state, { purchased: false });
    case actionTypes.PURCHASE_BURGER_START:
      return updateObject(state, { isError: false, isLoading: true });
    case actionTypes.PURCHASE_BURGER_SUCCESS:
      const newOrder = {
        ...action.payload.orderData,
        id: action.payload.orderId,
      };
      return updateObject(state, {
        isError: false,
        isLoading: false,
        purchased: true,
        orders: [...state.orders, newOrder],
      });
    case actionTypes.PURCHASE_BURGER_FAIL:
      return updateObject(state, { isError: true, isLoading: false });
    // case actionTypes.FETCH_ORDER_INIT:
    //   return updateObject(state, { isLoading: true, isError: false });
    // case actionTypes.FETCH_ORDER_FAIL:
    //   return updateObject(state, { isLoading: false, isError: true });
    case actionTypes.FETCH_ORDER_SUCCESS:
      return updateObject(state, {
        isLoading: false,
        isError: false,
        orders: action.payload,
      });
    default:
      return state;
  }
};

export default reducer;
