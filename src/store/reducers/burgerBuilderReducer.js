import * as actionTypes from "../actions/actionTypes";
import Constants from "../../util/constants/constants.json";
import { updateObject } from "../../util/function/utility";

const initialState = {
  ingredients: null,
  totalPrice: 0,
  isLoading: false,
  isError: false,
  building: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_INGREDIENTS_INIT:
      return updateObject(state, { isLoading: true, isError: false });
    case actionTypes.FETCH_INGREDIENTS_SUCCESS:
      return updateObject(state, {
        isLoading: false,
        isError: false,
        ingredients: action.payload,
        totalPrice: 0,
        building: false,
      });
    case actionTypes.FETCH_INGREDIENTS_FAIL:
      return updateObject(state, {
        isLoading: false,
        isError: true,
      });
    case actionTypes.GET_INGREDIENT:
      return updateObject(state, { ingredients: action.payload });
    case actionTypes.UPDATE_INGREDIENT:
      const { ingredient, isIncreasing } = action.payload;
      if (!isIncreasing && state.ingredients[ingredient] > 0) {
        state.ingredients[ingredient]--;
        state.totalPrice -= Constants.INGREDIENT_PRICES[ingredient];
      } else if (isIncreasing) {
        state.ingredients[ingredient]++;
        state.totalPrice += Constants.INGREDIENT_PRICES[ingredient];
      }
      state.building = true;
      return { ...state };
    default:
      return state;
  }
};

export default reducer;
